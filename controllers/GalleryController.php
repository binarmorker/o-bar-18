<?php

namespace Apine\Controllers\User;

use Apine\Core\Request;
use Apine\Exception\GenericException;
use Apine\Modules\Gallery\Factory\ObarUserFactory;
use Apine\Modules\Gallery\Post;
use Apine\Modules\Gallery\Element;
use Apine\Modules\Gallery\Factory\PostFactory;
use Apine\Modules\Gallery\PseudoCrypt;
use Apine\Modules\Gallery\SubscriptionHelper;
use Apine\MVC\APIActionsInterface;
use Apine\MVC\Controller;
use Apine\MVC\JSONView;
use Apine\MVC\TwigView;
use Apine\MVC\URLHelper;
use Apine\Session\SessionManager;

class GalleryController extends Controller implements APIActionsInterface {
	
	// WEB
	
	public function index($params) {
        $view = new TwigView();
		$view->set_view('gallery');
        $view->set_param('is_admin', ObarUserFactory::isAdmin(SessionManager::get_user()));

		if (isset($params['registration_success'])) {
			$view->set_param('registration_success', true);
		}

		return $view;
	}

    public function upload() {
        $view = new TwigView();

        if (SessionManager::is_logged_in()) {
            $view->set_view('upload');
            $view->set_param('is_admin', ObarUserFactory::isAdmin(SessionManager::get_user()));
        } else {
            throw new GenericException("Forbidden", 403);
        }

        return $view;
    }
	
	// API
	
	public function get($params) {
		$view = new JSONView();
		$response['previews'] = array();

        if (isset($params['count']) && !empty($params['count'])) {
            $is_admin = ObarUserFactory::isAdmin(SessionManager::get_user());
            $count = $params['count'];

            if (isset($params['last_date']) && !empty($params['last_date'])) {
                $last_date = $params['last_date'];
            } else {
                $last_date = null;
            }

            $previews = PostFactory::create_previews($is_admin, $count, $last_date, ($params['mature'] == 'true' || $params['mature'] == 1));

            if ($previews != null) {
                foreach ($previews as $item) {
                    $response['previews'][] = array(
                        'id' => PseudoCrypt::hash($item->get_id()),
                        'image' => $item->get_image(),
                        'type' => $item->get_type(),
                        'count' => $item->get_count(),
                        'name' => $item->get_name(),
                        'mature' => $item->get_mature(),
                        'removed' => $item->get_removed()
                    );
                    $response['last_date'] = $item->get_publication_date();
                }
            }
        }
		
		$view->set_json_file($response);
		$view->set_response_code(200);
		return $view;
	}
	
	public function post($params) {
		$view = new JSONView();

        if (!SessionManager::get_instance()->is_logged_in()) {
            throw new GenericException("You must be logged in to perform this action", 403);
        }

        foreach (explode('&', Request::get_request_body()) as $item) {
            $split = explode('=', $item);
            $params[reset($split)] = urldecode(end($split));
        }
		
		$response['postUrl'] = "";
		
		if (isset($params['elements']) && isset($params['title'])) {
			$post = new Post();
			$title = substr($params['title'], 0, 70);
			$description = substr($params['description'], 0, 1500);
			$post->set_name($title);
			$post->set_description($description);
			$post->set_mature($params['mature']);
			$post->set_author(SessionManager::get_instance()->get_user_id());
			$post->save();
			SubscriptionHelper::CreatePost($post);

			foreach ($params['elements'] as $item) {
				$element = new Element();
				$element->set_file($item->filename);
				$element->set_type($item->filetype);
				$element->set_post($post);
				$element->save();
			}

			$response['postUrl'] = URLHelper::path('post/') . PseudoCrypt::hash($post->get_id());
			$view->set_json_file($response);
			$view->set_response_code(200);
			return $view;
		}

		throw new GenericException("Bad Request", 400);
	}
	
	public function put($params) {
		throw new GenericException("Method Not Allowed", 405);
	}
	
	public function delete($params) {
		throw new GenericException("Method Not Allowed", 405);
	}
}