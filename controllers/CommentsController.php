<?php

namespace Apine\Controllers\User;

use Apine\Application\Config;
use Apine\Core\Request;
use Apine\Exception\GenericException;
use Apine\Modules\Gallery\Enums\PublicationType;
use Apine\Modules\Gallery\Factory\ObarUserFactory;
use Apine\Modules\Gallery\Factory\PostFactory;
use Apine\Modules\Gallery\PseudoCrypt;
use Apine\Modules\Gallery\SubscriptionHelper;
use Apine\Modules\Gallery\Comment;
use Apine\Modules\Gallery\Factory\CommentFactory;
use Apine\MVC\APIActionsInterface;
use Apine\MVC\JSONView;
use Apine\MVC\URLHelper;
use Apine\Session\SessionManager;
use Apine\User\Factory\UserFactory;

class CommentsController implements APIActionsInterface {
	
	public function get($params) {
		$view = new JSONView();
		$response['comments'] = array();
		$is_admin = ObarUserFactory::isAdmin(SessionManager::get_user());
		$comments = CommentFactory::create_for_post(PseudoCrypt::unhash($params[0]), array('desc' => false, 'is_admin' => $is_admin));
		
		if ($comments != null) {
			foreach ($comments as $item) {
                $username = null;
                $avatar = null;

                if (!Config::get('runtime', 'privacy')) {
                    if (UserFactory::is_id_exist($item->get_author())) {
                        $user = UserFactory::create_by_id($item->get_author());
                        $username = $user->get_username();
                        $avatar = $user->get_property('avatar');

                        if (explode(':', $avatar)[0] != 'data') {
                            $avatar = URLHelper::resource($avatar);
                        }
                    } else {
                        $username = "Deleted";
                    }
                }

                $file = $item->get_image();

                if ($file != null) {
                    $file_parts = explode('/', $file);
                    $file_path = $_SERVER["DOCUMENT_ROOT"] . '/resources/public/upload/' . end($file_parts);
                    $size = getimagesize($file_path);
                } else {
                    $size = array(0, 0);
                }

                $newComment = array(
					'id' => $item->get_id(),
                    'author' => $username,
                    'avatar' => $avatar,
					'comment' => html_entity_decode($item->get_comment()),
                    'image' => $file,
					'width' => $size[0],
					'height' => $size[1],
					'publication_date' => $item->get_publication_date(),
                    'removed' => $item->get_removed(),
                    'upvotes' => $item->get_upvotes(),
                    'downvotes' => $item->get_downvotes(),
                    'can_edit' => SessionManager::is_logged_in() && (SessionManager::get_user_id() == $item->get_author()),
                    'vote' => null
				);

                if (SessionManager::get_instance()->is_logged_in()) {
                    $user_vote = PostFactory::get_user_vote_for_post(PublicationType::Comment, $item->get_id(), SessionManager::get_instance()->get_user_id());

                    if ($user_vote != null) {
                        $newComment['vote'] = intval($user_vote);
                    }
                }

                $response['comments'][] = $newComment;
			}
		}
		
		$view->set_json_file($response);
		$view->set_response_code(200);
		return $view;
	}
	
	public function post($params) {
		$view = new JSONView();

        if (!SessionManager::get_instance()->is_logged_in()) {
            throw new GenericException("Forbidden", 403);
        }

		foreach (explode('&', Request::get_request_body()) as $item) {
			$split = explode('=', $item);
			$params[reset($split)] = urldecode(end($split));
		}
		
		if ($params != null) {
            $comment = new Comment();
            $comment->set_post(PseudoCrypt::unhash($params['post']));
            $comment->set_author(SessionManager::get_instance()->get_user_id());

            if ($params['comment'] != '') {
                $content = substr($params['comment'], 0, 150);
            } else {
                $content = ' ';
            }

            $comment->set_comment($content);

            if (isset($params['image'])) {
                $comment->set_image($params['image']);
            }

            $comment->save();
            $comment = CommentFactory::create_by_id($comment->get_id());
            SubscriptionHelper::CreateComment($comment);
            $username = null;

            if (!Config::get('runtime', 'privacy')) {
                if (UserFactory::is_id_exist($comment->get_author())) {
                    $user = UserFactory::create_by_id($comment->get_author());
                    $username = $user->get_username();
                } else {
                    $username = "Deleted";
                }
            }

		    $response['comment'] = array(
                'id' => $comment->get_id(),
                'author' => $username,
                'comment' => html_entity_decode($comment->get_comment()),
                'image' => $comment->get_image(),
                'publication_date' => $comment->get_publication_date(),
                'can_edit' => SessionManager::is_logged_in() && (SessionManager::get_user_id() == $comment->get_author())
            );
		} else {
		    throw new GenericException("Bad request", 400);
		}
		
        $view->set_json_file($response);
        $view->set_response_code(200);
		return $view;
	}
	
	public function put($params) {
		$view = new JSONView();

        if (!SessionManager::get_instance()->is_logged_in()) {
            throw new GenericException("Forbidden", 403);
        }

        foreach (explode('&', Request::get_request_body()) as $item) {
            $split = explode('=', $item);
            $params[reset($split)] = urldecode(end($split));
        }
		
		if (isset($params['id'])) {
            $is_admin = ObarUserFactory::isAdmin(SessionManager::get_user());
            $comment = CommentFactory::create_by_id($params['id'], array('is_admin' => $is_admin));

            if (isset($params['comment'])) {
                $content = substr($params['comment'], 0, 150);
                $comment->set_comment($content);
            }

            if (isset($params['removed'])) {
                if ($is_admin) {
                    $comment->set_removed(filter_var($params['removed'], FILTER_VALIDATE_BOOLEAN));
                }
            }

            $comment->save();
		    $response['comment'] = array(
                'id' => $comment->get_id(),
                'author' => Config::get('runtime', 'privacy') ? null : $comment->get_author(),
                'comment' => html_entity_decode($comment->get_comment()),
                'image' => $comment->get_image(),
                'publication_date' => $comment->get_publication_date(),
                'can_edit' => SessionManager::is_logged_in() && (SessionManager::get_user_id() == $comment->get_author())
            );
		} else {
		    throw new GenericException("Bad request", 400);
		}
		
        $view->set_json_file($response);
        $view->set_response_code(200);
		return $view;
	}
	
	public function delete($params) {
		throw new GenericException("Method Not Allowed", 405);
	}
}