<?php

namespace Apine\Controllers\User;

use Apine\Core\Request;
use Apine\Exception\GenericException;
use Apine\Modules\Gallery\PseudoCrypt;
use Apine\Modules\Gallery\Report;
use Apine\MVC\APIActionsInterface;
use Apine\MVC\JSONView;
use Apine\Session\SessionManager;

class ReportController implements APIActionsInterface {
	
	// API
	
	public function get($params) {
		$view = new JSONView();
		$response['terms'] = file_get_contents('termsandconditions.md');
		$response['privacy'] = file_get_contents('privacypolicy.md');
		$view->set_json_file($response);
		$view->set_response_code(200);
		return $view;
	}
	
	public function post($params) {
        $view = new JSONView();

        if (SessionManager::get_instance()->is_logged_in()) {
            $user = SessionManager::get_instance()->get_user()->get_id();
        } else {
            $user = Request::server()['REMOTE_ADDR'];
        }

        foreach (explode('&', Request::get_request_body()) as $item) {
            $split = explode('=', $item);
            $params[reset($split)] = urldecode(end($split));
        }

        if (!is_int($params['item_id'])) {
            $params['item_id'] = PseudoCrypt::unhash($params['item_id']);
        }

        if ($params['item_id'] != null) {
            $report = new Report();
            $report->set_type($params['type']);
            $report->set_item_id($params['item_id']);
            $report->set_author($user);
            $report->set_description($params['description']);
            $report->save();
        } else {
            throw new GenericException("Bad request", 400);
        }

        $view->set_json_file(array("message" => "Ok"));
        $view->set_response_code(200);
        return $view;
	}
	
	public function put($params) {
		throw new GenericException("Method Not Allowed", 405);
	}
	
	public function delete($params) {
		throw new GenericException("Method Not Allowed", 405);
	}
}