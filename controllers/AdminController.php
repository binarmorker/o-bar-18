<?php

namespace Apine\Controllers\User;

use Apine\Core\Collection;
use Apine\Core\Database;
use Apine\Core\Request;
use Apine\Modules\Gallery\EmailHelper;
use Apine\Modules\Gallery\Enums\PublicationType;
use Apine\Modules\Gallery\Factory\CommentFactory;
use Apine\Modules\Gallery\Factory\ObarUserFactory;
use Apine\Modules\Gallery\Factory\PostFactory;
use Apine\Modules\Gallery\Factory\ReportFactory;
use Apine\Modules\Gallery\PseudoCrypt;
use Apine\MVC\Controller;
use Apine\MVC\TwigView;
use Apine\Session\SessionManager;
use Apine\Exception\GenericException;
use Apine\User\Factory\UserFactory;
use Apine\User\Factory\UserGroupFactory;
use Apine\User\User;
use Apine\User\UserGroup;
use Apine\Utility\Routes;
use Identicon\Identicon;
use stdClass;

class AdminController extends Controller {
	
	// INDEX
	
	public function index() {
        $view = new TwigView();

        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        $view->set_param('member_count', ObarUserFactory::get_count());
        $view->set_param('groups_count', UserGroupFactory::create_all()->length());
        $view->set_param('reports_count', ReportFactory::get_count());

        $view->set_param('posts_count', PostFactory::get_count());
        $view->set_param('comments_count', CommentFactory::get_count());
        $view->set_param('bans_count', 0);

        $database = new Database();
        $sessionCount = $database->select("SELECT COUNT(*) as `count` FROM apine_sessions");
        $view->set_param('sessions_count', $sessionCount[0]['count']);

        $view->set_view('admin/index');
		return $view;
	}

    public function setadmin($params) {
        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        $user = SessionManager::get_user();
        $user->set_property('admin', isset($params['isAdmin']));
        $user->save();

        return Routes::internal_redirect($params['route']);
    }

    // MEMBER

    public function members($params) {
        $view = new TwigView();

        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        if (Request::is_post()) {
            $key = array_keys($params)[0];
            $id = explode('-', $key)[1];

            if (UserFactory::is_id_exist($id)) {
                $user = UserFactory::create_by_id($id);
                $user->set_username($params["username-$id"]);
                $user->set_email_address($params["email-$id"]);
                $user->set_property('avatar', $params["avatar-$id"]);
                $user->set_property('bio', $params["bio-$id"]);
                $user->set_property('cover', $params["cover-$id"]);
                $user->set_property('realname', $params["realname-$id"]);
                $user->set_property('mature', array_key_exists("mature-$id", $params));
                $user->set_property('mask_mature', array_key_exists("mask_mature-$id", $params));
                $groups = new Collection();
                $groupIds = array_filter(array_keys($params), function($param) {
                    return strpos($param, 'group') > -1;
                });

                foreach ($groupIds as $group) {
                    $groupId = explode('-', $group)[1];

                    if (UserGroupFactory::is_id_exist($groupId)) {
                        $groups->add_item(UserGroupFactory::create_by_id($groupId));
                    }
                }

                $user->set_group($groups);
                $user->save();
            } else {
                throw new GenericException('User not found', 404);
            }
        }

        $view->set_param('members', UserFactory::create_all());
        $view->set_param('groups', UserGroupFactory::create_all());

        $view->set_view('admin/members');
        return $view;
    }

    public function newmember($params) {
        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        if (Request::is_post()) {
            if (!UserFactory::is_name_exist($params["username-new"]) && !UserFactory::is_email_exist($params["email-new"])) {
                $user = new User();
                $user->set_username($params["username-new"]);
                $user->set_email_address($params["email-new"]);
                $user->set_password("");
                $user->set_type(APINE_SESSION_USER);

                $identicon = new Identicon();
                $image = $identicon->getImageDataUri($user->get_email_address(), 250);

                $user->set_property('avatar', $image);
                $user->set_property('cover', $params["cover-new"]);
                $user->set_property('realname', $params["realname-new"]);
                $user->set_property('bio', $params["bio-new"]);
                $user->set_property('mature', array_key_exists("mature-new", $params));
                $user->set_property('mask_mature', array_key_exists("mask_mature-new", $params));
                $groups = new Collection();
                $groupIds = array_filter(array_keys($params), function ($param) {
                    return strpos($param, 'group') > -1;
                });

                foreach ($groupIds as $group) {
                    $groupId = explode('-', $group)[1];

                    if (UserGroupFactory::is_id_exist($groupId)) {
                        $groups->add_item(UserGroupFactory::create_by_id($groupId));
                    }
                }

                $user->set_group($groups);
                $user->save();
            } else {
                throw new GenericException('User already exists', 409);
            }
        }

        return Routes::internal_redirect('/admin/members');
    }

    public function passwordreset($params) {
        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        if (UserFactory::is_id_exist($params[0])) {
            $user = UserFactory::create_by_id($params[0]);
            EmailHelper::send_recovery_email($user);
        } else {
            throw new GenericException('User not found', 404);
        }

        return Routes::internal_redirect('/admin/members');
    }

    public function avatarreset($params) {
        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        if (UserFactory::is_id_exist($params[0])) {
            $user = UserFactory::create_by_id($params[0]);
            $identicon = new Identicon();
            $image = $identicon->getImageDataUri($user->get_email_address(), 250);

            $user->set_property('avatar', $image);
            $user->save();
        } else {
            throw new GenericException('User not found', 404);
        }

        return Routes::internal_redirect('/admin/members');
    }

    public function accountreset() {
	    throw new GenericException('Not Implemented', 501);
    }

    // GROUPS

    public function groups($params) {
        $view = new TwigView();

        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        $groups = array();
        $members = UserFactory::create_all();

        if (Request::is_post()) {
            $key = array_keys($params)[0];
            $id = explode('-', $key)[1];

            if (UserGroupFactory::is_id_exist($id)) {
                $group = UserGroupFactory::create_by_id($id);
                $group->set_name($params["name-$id"]);
                $group->save();

                $memberIds = array_filter(array_keys($params), function($param) {
                    return strpos($param, 'member') > -1;
                });

                foreach ($members as $member) {
                    $memberGroups = $member->get_group();

                    if ($memberGroups instanceof Collection) {
                        $add = false;

                        foreach ($memberIds as $memberId) {
                            if ($member->get_id() == explode('-', $memberId)[1]) {
                                $add = true;
                            }
                        }

                        if ($add) {
                            if (!$member->has_group($group->get_id())) {
                                $memberGroups->add_item($group);
                            }
                        } else {
                            if ($member->has_group($group->get_id())) {
                                foreach ($memberGroups as $key => $memberGroup) {
                                    if ($memberGroup->get_id() == $group->get_id()) {
                                        $memberGroups->remove_item($key);
                                    }
                                }
                            }
                        }

                        $member->set_group($memberGroups);
                        $member->save();
                    }
                }
            } else {
                throw new GenericException('Group not found', 404);
            }
        }

        foreach (UserGroupFactory::create_all() as $group) {
            $groups[] = array(
                'count' => UserFactory::create_by_group($group->get_id())->length(),
                'group' => $group
            );
        }

        $view->set_param('groups', $groups);
        $view->set_param('members', $members);

        $view->set_view('admin/groups');
        return $view;
    }

    public function newgroup($params) {
        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        $members = UserFactory::create_all();

        if (Request::is_post()) {
            $group = new UserGroup();
            $group->set_name($params["name-new"]);
            $group->save();

            $memberIds = array_filter(array_keys($params), function($param) {
                return strpos($param, 'member') > -1;
            });

            foreach ($members as $member) {
                $memberGroups = $member->get_group();

                if ($memberGroups instanceof Collection) {
                    foreach ($memberIds as $memberId) {
                        if ($member->get_id() == explode('-', $memberId)[1]) {
                            $memberGroups->add_item($group);
                        }
                    }

                    $member->set_group($memberGroups);
                    $member->save();
                }
            }
        }

        return Routes::internal_redirect('/admin/groups');
    }

    public function removegroup() {
        throw new GenericException('Not Implemented', 501);
    }

    // REPORTS

    public function reports() {
        $view = new TwigView();

        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        $reports = array();

        foreach (ReportFactory::create_all() as $report) {
            $id = 0;

            switch ($report->get_type()) {
                case PublicationType::Post:
                    $id = $report->get_item_id();
                    break;
                case PublicationType::Comment:
                    $id = $report->get_publication()->get_post();

                    break;
            }

            $reports[] = array(
                'id' => PseudoCrypt::hash($id),
                'report' => $report
            );
        }

        $view->set_param('members', UserFactory::create_all());
        $view->set_param('reports', $reports);

        $view->set_view('admin/reports');
        return $view;
    }

    public function resolvereport($params) {
        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        if (ReportFactory::is_id_exist($params[0])) {
            $report = ReportFactory::create_by_id($params[0]);
            $report->set_resolved(true);
            $report->save();
        } else {
            throw new GenericException('Report not found', 404);
        }

        return Routes::internal_redirect('/admin/reports');
    }

    public function unresolvereport($params) {
        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        if (ReportFactory::is_id_exist($params[0])) {
            $report = ReportFactory::create_by_id($params[0]);
            $report->set_resolved(false);
            $report->save();
        } else {
            throw new GenericException('Report not found', 404);
        }

        return Routes::internal_redirect('/admin/reports');
    }

    public function removepublication($params) {
        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        if (ReportFactory::is_id_exist($params[0])) {
            $report = ReportFactory::create_by_id($params[0]);

            switch ($report->get_type()) {
                case PublicationType::Post:
                    if (PostFactory::is_id_exist($report->get_item_id(), true)) {
                        if (PostFactory::is_id_exist($report->get_item_id())) {
                            $post = PostFactory::create_by_id($report->get_item_id());
                            $post->set_removed(true);
                            $post->save();
                        } else {
                            throw new GenericException('Post already removed', 401);
                        }
                    } else {
                        throw new GenericException('Post not found', 404);
                    }

                    break;
                case PublicationType::Comment:
                    if (CommentFactory::is_id_exist($report->get_item_id(), true)) {
                        if (CommentFactory::is_id_exist($report->get_item_id())) {
                            $post = CommentFactory::create_by_id($report->get_item_id());
                            $post->set_removed(true);
                            $post->save();
                        } else {
                            throw new GenericException('Post already removed', 401);
                        }
                    } else {
                        throw new GenericException('Post not found', 404);
                    }

                    break;
            }

            $report->set_resolved(true);
            $report->save();
        } else {
            throw new GenericException('Report not found', 404);
        }

        return Routes::internal_redirect('/admin/reports');
    }

    public function banuser() {
        throw new GenericException('Not Implemented', 501);
    }

    // SESSIONS

    public function sessions() {
        $view = new TwigView();

        if (!SessionManager::is_logged_in() || !SessionManager::get_user()->has_group(3)) {
            throw new GenericException("Forbidden", 403);
        }

        $database = new Database();
        $sessions = array_map(function($item) {
            $session = new stdClass();
            $session->id = $item['id'];
            $session->last_access = $item['last_access'];
            $session->data = json_encode(json_decode($item['data']), JSON_PRETTY_PRINT);
            return $session;
        }, $database->select("SELECT * FROM apine_sessions ORDER BY `last_access` DESC"));

        $view->set_param('sessions', $sessions);

        $view->set_view('admin/sessions');
        return $view;
    }
}