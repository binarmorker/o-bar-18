<?php

namespace Apine\Controllers\User;

use Apine\Core\Database;
use Apine\Core\Request;
use Apine\MVC\Controller;
use Apine\MVC\JSONView;
use Apine\Session\SessionManager;
use Apine\User\Property;
use Exception;

class LoggerController extends Controller {

    public function index() {
        $session = SessionManager::get_user();
        $body = Request::get_request_body();
        $data = json_decode($body, JSON_OBJECT_AS_ARRAY);
        $data['server'] = Request::server();

        try {
            $json = json_decode($data['response']);
            $data['response'] = $json;
        } catch (Exception $e) {}

        if ($session != null) {
            $data['session'] = [
                "id" => $session->get_id(),
                "username" => $session->get_username(),
                "type" => $session->get_type(),
                "groups" => $session->get_group()->get_all(),
                "email_address" => $session->get_email_address(),
                "register_date" => $session->get_register_date(),
                "properties" => array_map(function(Property $item) {
                    return [ 'id' => $item->get_id(), 'name' => $item->get_name(), 'value' => unserialize($item->get_value()) ];
                }, $session->get_property_all())
            ];
        }

        $database = new Database();
        $database->insert('obar_errors', [ 'json' => json_encode($data) ]);

        return new JSONView();
    }
}