<?php

namespace Apine\Controllers\User;

use Apine\Core\Request;
use Apine\Exception\GenericException;
use Apine\Modules\Gallery\Factory\ObarUserFactory;
use Apine\Modules\Gallery\Factory\PostFactory;
use Apine\Modules\Gallery\PseudoCrypt;
use Apine\MVC\APIActionsInterface;
use Apine\MVC\Controller;
use Apine\MVC\JSONView;
use Apine\MVC\TwigView;
use Apine\MVC\URLHelper;
use Apine\Session\SessionManager;
use Apine\Application\Config;
use Apine\User\Factory\UserFactory;
use Apine\User\UserGroup;

class ProfileController extends Controller implements APIActionsInterface {
	
	// WEB

    public function index($params) {

        $user = null;

        if (isset($params[0])) {
            $user = UserFactory::create_by_name($params[0]);

            if ($user == null || Config::get('runtime', 'privacy')) {
                throw new GenericException("Not Found", 404);
            }
        } else {
            $user = SessionManager::get_user();
        }

        $view = new TwigView();
        $view->set_view('profile');
        $view->set_param('username', $user->get_username());
        $view->set_param('is_admin', ObarUserFactory::isAdmin(SessionManager::get_user()));
        return $view;
    }
	
	// API
	
	public function get($params) {
		$view = new JSONView();
		$response = array();
        $user = UserFactory::create_by_name($params['user']);
        $is_admin = ObarUserFactory::isAdmin(SessionManager::get_user());
        $is_self = false;

        if ($user != null && $user->get_id() == SessionManager::get_user_id()) {
            $is_self = true;
        }

        if (!$is_self && Config::get('runtime', 'privacy')) {
            throw new GenericException('Not Found', 404);
        }

        $response['user']['is_self'] = $is_self;
        $response['user']['username'] = $user->get_username();
        $response['user']['avatar'] = $user->get_property('avatar');

        if (explode(':', $response['user']['avatar'])[0] != 'data') {
            $response['user']['avatar'] = URLHelper::resource($response['user']['avatar']);
        }

        $response['user']['realname'] = !is_null($user->get_property('realname')) ? $user->get_property('realname') : '';
        $response['user']['cover'] = !is_null($user->get_property('cover')) ? $user->get_property('cover') : '';
        $response['user']['bio'] = !is_null($user->get_property('bio')) ? $user->get_property('bio') : '';
        $response['user']['mask_mature'] = !!$user->get_property('mask_mature');

        if ($is_self || $is_admin) {
            $response['user']['email'] = $user->get_email_address();
        }

        $response['user']['registration_date'] = $user->get_register_date();
        $response['user']['posts'] = PostFactory::get_user_post_count($user->get_id());
        $response['user']['groups'] = implode(', ', array_map(function (UserGroup $value) {
            return $value->get_name();
        }, array_reverse($user->get_group()->get_all())));

        $received = PostFactory::get_user_received_reputation($user->get_id());

        if ($received['total'] > 0) {
            $response['user']['reputation'] = ($received['upvotes'] / $received['total']) - ($received['downvotes'] / $received['total']);
        } else {
            $response['user']['reputation'] = 0;
        }

        $given = PostFactory::get_user_given_reputation($user->get_id());

        if ($given['total'] > 0) {
            $response['user']['temper'] = ($given['upvotes'] / $given['total']) - ($given['downvotes'] / $given['total']);
        } else {
            $response['user']['temper'] = 0;
        }

        if (isset($params['count']) && !empty($params['count'])) {
            $count = $params['count'];

            if (isset($params['last_date']) && !empty($params['last_date'])) {
                $last_date = $params['last_date'];
            } else {
                $last_date = null;
            }

            $previews = PostFactory::create_previews_for_user($user->get_id(), $is_admin, $count, $last_date, ($params['mature'] == 'true' || $params['mature'] == 1));

            if ($previews != null) {
                foreach ($previews as $item) {
                    $response['previews'][] = array(
                        'id' => PseudoCrypt::hash($item->get_id()),
                        'image' => $item->get_image(),
                        'name' => $item->get_name(),
                        'type' => $item->get_type(),
                        'count' => $item->get_count(),
                        'mature' => $item->get_mature(),
                        'removed' => $item->get_removed()
                    );
                    $response['last_date'] = $item->get_publication_date();
                }
            }
        }
		
		$view->set_json_file($response);
		$view->set_response_code(200);
		return $view;
	}
	
	public function post($params) {
		$view = new JSONView();

        if (!SessionManager::get_instance()->is_logged_in()) {
            throw new GenericException("You must be logged in to perform this action", 403);
        }

        foreach (explode('&', Request::get_request_body()) as $item) {
            $split = explode('=', $item);
            $params[reset($split)] = urldecode(end($split));
        }

        if (isset($params['mature'])) {
            $user = SessionManager::get_user();
            $user->set_property('mature', $params['mature']);
            $user->save();
        }

        return $view;
	}
	
	public function put($params) {
		throw new GenericException("Method Not Allowed", 405);
	}
	
	public function delete($params) {
		throw new GenericException("Method Not Allowed", 405);
	}
}
