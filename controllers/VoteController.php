<?php

namespace Apine\Controllers\User;

use Apine\Application\Config;
use Apine\Core\Request;
use Apine\Exception\GenericException;
use Apine\Modules\Gallery\Enums\PublicationType;
use Apine\Modules\Gallery\Factory\CommentFactory;
use Apine\Modules\Gallery\PseudoCrypt;
use Apine\Modules\Gallery\SubscriptionHelper;
use Apine\MVC as MVC;
use Apine\MVC\URLHelper;
use Apine\Session\SessionManager;
use Apine\Modules\Gallery\Factory\PostFactory;
use Apine\User\Factory\UserFactory;

class VoteController implements MVC\APIActionsInterface {
	
	public function get($params) {
        $view = new MVC\JSONView();

        if (!Config::get('runtime', 'privacy')) {
            $response['votes'] = [];

            if ($params != null) {
                $response['votes'] = array();
                $votes = PostFactory::get_votes_for_post(PseudoCrypt::unhash($params[0]));

                foreach ($votes as $vote) {
                    $user = UserFactory::create_by_id($vote['author']);
                    $avatar = $user->get_property('avatar');

                    if (explode(':', $avatar)[0] != 'data') {
                        $avatar = URLHelper::resource($avatar);
                    }

                    $response['votes'][] = array(
                        'name' => $user->get_username(),
                        'icon' => $avatar,
                        'type' => $vote['upvote']
                    );
                }

                $view->set_json_file($response);
                $view->set_response_code(200);
            } else {
                throw new GenericException("No parameters", 400);
            }

            return $view;
        } else {
            throw new GenericException("Method Not Allowed", 405);
        }
	}
	
	public function post($params) {
        $view = new MVC\JSONView();

        if (!SessionManager::get_instance()->is_logged_in()) {
            throw new GenericException("You must be logged in to perform this action", 403);
        }

		$response['created'] = true;

        foreach (explode('&', Request::get_request_body()) as $item) {
            $split = explode('=', $item);
            $params[reset($split)] = urldecode(end($split));
        }
		
		if ($params != null) {
		    switch ($params['type']) {
                case PublicationType::Post:
                    PostFactory::apply_vote($params['type'], $params['vote'], PseudoCrypt::unhash($params['post']), SessionManager::get_instance()->get_user_id());
                    SubscriptionHelper::CreateVote(PostFactory::create_by_id(PseudoCrypt::unhash($params['post'])), $params['vote']);
                    $view->set_json_file($response);
                    $view->set_response_code(200);
                    break;
                case PublicationType::Comment:
                    PostFactory::apply_vote($params['type'], $params['vote'], $params['post'], SessionManager::get_instance()->get_user_id());
                    SubscriptionHelper::CreateVote(CommentFactory::create_by_id($params['post']), $params['vote']);
                    $view->set_json_file($response);
                    $view->set_response_code(200);
                    break;
                default:
                    throw new GenericException("Bad parameters", 400);
            }
		} else {
		    throw new GenericException("No parameters", 400);
		}
		
		return $view;
	}
	
	public function put($params) {
		throw new GenericException("Method Not Allowed", 405);
	}
	
	public function delete($params) {
		throw new GenericException("Method Not Allowed", 405);
	}
}