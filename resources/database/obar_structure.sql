SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE `obar_comments` (
  `id` int(11) NOT NULL,
  `post` int(11) NOT NULL,
  `author` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `publication_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `removed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `obar_elements` (
  `id` int(11) NOT NULL,
  `post` int(11) NOT NULL,
  `file` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `removed` tinyint(1) NOT NULL DEFAULT '0',
  `reported` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `obar_errors` (
  `id` int(11) NOT NULL,
  `json` json NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `obar_messaging` (
  `user_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `obar_notifications` (
  `user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `publication_id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data` text COLLATE utf8mb4_bin NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `obar_posts` (
  `id` int(11) NOT NULL,
  `name` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` int(11) NOT NULL DEFAULT '0',
  `publication_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `mature` tinyint(1) NOT NULL DEFAULT '0',
  `removed` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `obar_reports` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `author` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` text COLLATE utf8mb4_bin,
  `publication_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `resolved` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `obar_subscriptions` (
  `user` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `publication` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

CREATE TABLE `obar_votes` (
  `post` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `author` int(11) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `upvote` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `obar_comments`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `obar_elements`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `obar_errors`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `obar_messaging`
  ADD PRIMARY KEY (`token`);

ALTER TABLE `obar_notifications`
  ADD PRIMARY KEY (`user_id`,`type`,`publication_id`);

ALTER TABLE `obar_posts`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `obar_reports`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `obar_subscriptions`
  ADD PRIMARY KEY (`user`,`type`,`publication`) USING BTREE;

ALTER TABLE `obar_votes`
  ADD PRIMARY KEY (`post`,`type`,`author`) USING BTREE;

ALTER TABLE `obar_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `obar_elements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `obar_errors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `obar_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  
ALTER TABLE `obar_reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
