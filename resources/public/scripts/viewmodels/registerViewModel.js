'use strict';

var RegisterViewModel = function() {
    var self = this;
    self.username = ko.observable(null);
    self.password = ko.observable(null);
    self.confirm = ko.observable(null);
    self.email = ko.observable(null);
    self.captcha = ko.observable(false);
    self.checkbox = ko.observable(false);

    self.submit = function(undefined, e) {
        e.preventDefault();
        API.Auth.Login({
            username: self.username(),
            password: Base64.encode(self.password())
        },
        function() {
            window.location.href = '/';
        },
        function() {
            new ErrorToast("Login failed");
        });
    };

    self.canSubmit = ko.computed(function() {
        return !(self.username() && self.password() && self.confirm() && self.email() && self.captcha() && self.checkbox());
    });

    self.load = function(grecaptcha, dark) {
        grecaptcha.render(
            "g-recaptcha",
            {
                "theme": dark ? "dark" : "light",
                "sitekey": "6Le6GyQTAAAAAOU4OiruaC462z2HnwwHngCwe9eB",
                "callback": function () {
                    self.captcha(true);
                },
                "expired-callback": function () {
                    self.captcha(false);
                }
            }
        );

        setLoading(false);
        setInitializing(false);
    }
};