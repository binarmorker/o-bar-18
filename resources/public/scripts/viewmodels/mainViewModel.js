'use strict';

var MainViewModel = function(user, hasNotifications) {
    var self = this;
    self.error = ko.observable(false);
    self.loading = ko.observable(true);
    self.initializing = ko.observable(true);
    self.upload = ko.observable(null);
    self.login = ko.observable(null);
    self.register = ko.observable(null);
    self.page = ko.observable(null);
    self.chat = ko.observable(null);
    self.defaultTitle = document.title;
    self.applicationName = self.defaultTitle.split(' - ')[0].trim();
    self.defaultPath = window.location.href;
    self.notifications = null;

    if (user) {
        if (hasNotifications) {
            self.notifications = new NotificationViewModel();
            self.notifications.load();
        }
    }

    self.showPage = function(viewmodel) {
        self.page(viewmodel);
        self.page().load();
    };

    self.resetTitle = function() {
        document.title = self.defaultTitle;
    };

    self.showMenu = function(undefined, event) {
        var menu = $("nav ul.menu");
        var $body = $("body");
        var menuClick = function(event) {
            if (($(event.target).closest('a').length > 0 && $(event.target).closest(".mobile-button").length === 0) || ($(event.target).closest(".mobile-button").length === 0 && $(event.target).closest(".menu").length === 0)) {
                menu.slideUp(500);
                $body.unbind("click", menuClick);
            }
        };

        if (menu.is(":visible")) {
            event.preventDefault();
            menu.slideUp(500);
            $body.unbind("click", menuClick);
        } else {
            event.preventDefault();
            menu.slideDown(500);
            $body.click(menuClick);
        }
    };

    self.showNotifications = function(undefined, event) {
        var menu = $("nav ul.notificationsMenu");
        var $body = $("body");
        var menuClick = function(event) {
            if ($(event.target).closest(".notificationsButton").length === 0 && $(event.target).closest(".notificationsMenu").length === 0) {
                menu.slideUp(500);
                $body.unbind("click", menuClick);
            }
        };

        if (menu.is(":visible")) {
            event.preventDefault();
            menu.slideUp(500);
            $body.unbind("click", menuClick);
        } else {
            event.preventDefault();
            menu.slideDown(500);
            $body.click(menuClick);
        }
    };

    self.showLogin = function() {
        self.login(new LoginViewModel());
        self.login().load();
        document.title = self.applicationName + ' - Login';
        history.pushState({action: "showLogin"}, document.title, '/login');
        var modal = new Modal('#login-modal', null, function() {
            history.pushState({ action: "close" }, self.defaultTitle, self.defaultPath);
            self.resetTitle();
        });
        modal.show();
        $('#login-modal').find('input').first().focus();
    };

    self.showRegister = function() {
        self.register(new RegisterViewModel());
        self.register().load(grecaptcha, true);
        document.title = self.applicationName + ' - Register';
        history.pushState({action: "showRegister"}, document.title, '/register');
        var modal = new Modal('#register-modal', null, function() {
            history.pushState({ action: "close" }, self.defaultTitle, self.defaultPath);
            self.resetTitle();
        });
        modal.show();
        $('#register-modal').find('input').first().focus();
    };

    self.showUpload = function() {
        self.upload(new UploadViewModel());
        self.upload().load();
        document.title = self.applicationName + ' - Upload';
        history.pushState({action: "showUpload"}, document.title, '/upload');
        var modal = new Modal('#upload-modal', null, function() {
            history.pushState({ action: "close" }, self.defaultTitle, self.defaultPath);
            $(document).unbind('paste');
            self.resetTitle();
        });
        modal.show();
    };
};