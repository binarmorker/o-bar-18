'use strict';

var MembersViewModel = function() {
    var self = this;
    self.page = 1;
    self.count = 0;
    self.members = ko.observableArray([]);
    self.shouldLoad = ko.observable(true);

    $(window).scroll(function() {
        if (self.shouldLoad() && $(window).scrollTop() + $(window).height() >= getDocumentHeight() - ($(window).height() * 0.75)) {
            self.load();
        }
    });

    self.load = function() {
        if (self.shouldLoad()) {
            self.shouldLoad(false);

            API.Members.Get(
                self.page,
                function (data) {
                    data.members.forEach(function (item) {
                        self.members.push(new Member(item));
                    });

                    var $grid = $('.grid');

                    if (self.page === 1) {
                        $grid.masonry({
                            transitionDuration: 0
                        });
                    }

                    self.count = data.count;

                    if (self.page * 30 <= self.count) {
                        self.shouldLoad(true);
                    }

                    self.page++;
                    setLoading(false);
                    setInitializing(false);

                    $grid.masonry('reloadItems');
                    $grid.masonry('layout');
                },
                function (data) {
                    new ErrorToast(data);
                    setLoading(false);
                    setInitializing(false);
                }
            );
        }
        ;
    }
};