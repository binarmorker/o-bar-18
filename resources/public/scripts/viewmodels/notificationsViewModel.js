'use strict';

var NotificationsViewModel = function() {
    var self = this;
    self.page = 1;
    self.count = 0;
    self.notifications = ko.observableArray([]);
    self.shouldLoad = ko.observable(false);

    $(window).scroll(function() {
        if (self.shouldLoad() && $(window).scrollTop() + $(window).height() >= getDocumentHeight() - ($(window).height() * 0.75)) {
            self.shouldLoad(false);
            self.load();
        }
    });

    self.load = function() {
        API.Notifications.Get(
            self.page,
            12,
            function (data) {
                data.notifications.forEach(function (item) {
                    self.notifications.push(GetNotificationMessage(item));
                });

                self.count = data.count;

                if (self.page * 8 < self.count) {
                    self.shouldLoad(true);
                }

                self.page++;
                setLoading(false);
                setInitializing(false);
            },
            function (data) {
                new ErrorToast(data);
                setLoading(false);
                setInitializing(false);
            }
        );
    };

    self.markRead = function(data) {
        data.seen(!data.seen());

        API.Notifications.Post(
            {
                'id': data.id(),
                'type': data.type(),
                'seen': data.seen()
            },
            function (data) {

            },
            function (data) {
                new ErrorToast(data);
            }
        );
    };
};