'use strict';

var ProfileViewModel = function(userName, isMature, mask) {
    var self = this;
    self.previews = ko.observableArray([]);
    self.post = ko.observable(null);
    self.last_date = ko.observable(null);
    self.shouldLoad = ko.observable(false);
    self.showMature = ko.observable(isMature);
    self.maskMature = ko.observable(mask);
    self.user = ko.observable();
    self.defaultTitle = document.title;
    self.applicationName = self.defaultTitle.split(' - ')[0].trim();
    self.password = ko.observable('');
    self.confirm = ko.observable('');
    self.dropzone = null;
    self.isImageUploaded = ko.observable(false);

    self.reputationText = function(value) {
        if (value <= -0.6) { return "Hated" }
        else if (value <= -0.2) { return "Disliked" }
        else if (value <= 0.2) { return "Neutral" }
        else if (value <= 0.6) { return "Liked" }
        else { return "Loved" }
    };

    self.temperText = function(value) {
        if (value <= -0.6) { return "Hater" }
        else if (value <= -0.2) { return "Downvoter" }
        else if (value <= 0.2) { return "Neutral" }
        else if (value <= 0.6) { return "Upvoter" }
        else { return "Attention Whore" }
    };

    self.showMature.subscribe(function(value) {
        $(window).off('scroll');
        $('.end-info').hide();
        self.last_date(null);
        self.shouldLoad(true);
        self.previews([]);
        self.load();
        API.Gallery.ShowMature({
            mature: value
        });
    });

    self.showPost = function(data) {
        self.post(new PostViewModel(data.id(), true, null, user));
        self.post().load();
        history.pushState({action: "showPost", data: {id: data.id(), name: data.name()}}, self.applicationName + ' - ' + data.name(), '/post/' + data.id());
        var modal = new Modal('#post-modal', null, function() {
            $("body").off("keydown");
            self.resetTitle();
            self.post(null);
            history.pushState({ action: "close" }, self.defaultTitle, '/profile' + (!self.user().is_self() ? '/' + userName : ''));
        });
        modal.show();
    };

    self.resetTitle = function() {
        document.title = self.defaultTitle;
    };

    self.passwordValid = ko.computed(function () {
        return (self.password().trim() !== "" && self.confirm().trim() !== "" && self.password() === self.confirm());
    });

    self.savePassword = function () {
        if (self.user().is_self()) {
            API.Auth.ChangePassword({
                    password: self.password(),
                    confirm: self.confirm()
                },
                function () {
                    new Success("Changed password successfully.");
                },
                function () {
                    new ErrorToast("An error occured.");
                },
                function () {
                    self.password('');
                    self.confirm('');
                }
            );
        }
    };

    self.save = function () {
        if (self.user().is_self()) {
            var args = {
                realname: self.user().realname(),
                bio: self.user().bio(),
                mask_mature: self.maskMature(),
                email: self.user().email()
            };

            if (self.isImageUploaded()) {
                args.avatar = $('#avatar').find('img').attr('src');
            }

            API.Auth.Update(
                args,
                function () {
                    new Success("Saved profile successfully.");
                    self.isImageUploaded(false);
                },
                function () {
                    new ErrorToast("An error occured.");
                }
            );
        }
    };

    self.showSettings = function () {
        if (self.user().is_self()) {
            var modal = new Modal('#profile-modal', null, null);
            modal.show();
            $('#profile-modal').find('input').first().focus();
        }
    };

    self.load = function() {
        var screenWidth = $(window).width();
        var count = 50;

        if (screenWidth < 959) {
            if (screenWidth < 549) {
                count = 15;
            } else {
                count = 30;
            }
        }

        API.Gallery.GetForUser(
            {
                user: userName,
                count: count,
                last_date: self.last_date(),
                mature: self.showMature()
            },
            function(data) {
                self.user(new Profile(data.user));

                if (self.user().is_self()) {
                    self.dropzone = new Dropzone('#setAvatar', {
                        addRemoveLinks: false,
                        previewsContainer: '#avatar',
                        previewTemplate: '<div class="dz-image"><img data-dz-thumbnail /></div>',
                        maxFiles: 1,
                        thumbnailHeight: 250,
                        thumbnailWidth: 250,
                        parallelUploads: 1,
                        uploadMultiple: false,
                        acceptedFiles: 'image/*',
                        maxFilesize: 100,
                        maxThumbnailFilesize: 25,
                        autoProcessQueue: false,
                        dictDefaultMessage: '<i class="zmdi zmdi-edit uploadIconUpload"></i><span class="close uploadIconDelete">× Remove</span>'
                    });

                    self.dropzone.on("addedfile", function () {
                        $('.dz-progress').hide();
                        self.isImageUploaded(self.dropzone.files.length > 0);
                        self.dropzone.removeEventListeners();
                        $('.uploadIconUpload').hide();
                        $('.uploadIconDelete').show();
                        $('#setAvatar').on('click', function () {
                            self.dropzone.removeAllFiles();
                        });
                    });

                    self.dropzone.on("removedfile", function () {
                        self.isImageUploaded(self.dropzone.files.length > 0);
                        self.dropzone.setupEventListeners();
                        $('#setAvatar').off('click');
                        $('.uploadIconUpload').show();
                        $('.uploadIconDelete').hide();
                    });
                }

                if (data.previews && data.previews.length > 0) {
                    data.previews.forEach(function(item) {
                        self.previews.push(new Preview(item));
                    });

                    $('.end-info').hide();
                    $('.no-info').hide();

                    $('.grid img').imagesLoaded(function () {
                        setLoading(false);
                        setInitializing(false);
                        var $grid = $('.grid');

                        if (self.last_date()) {
                            $grid.masonry('reloadItems');
                            $grid.masonry('layout')
                        } else {
                            $grid.masonry({
                                transitionDuration: 0
                            });
                        }

                        $grid.children('a').css('opacity', '1');
                        self.last_date(data.last_date);
                        self.shouldLoad(true);

                        $(window).scroll(function() {
                            if (self.shouldLoad() && $(window).scrollTop() + $(window).height() >= getDocumentHeight() - ($(window).height() * 0.75)) {
                                self.shouldLoad(false);
                                self.load();
                            }
                        });
                    });

                    if (self.maskMature()) {
                        $('.grid .mature img').each(function (index, elem) {
                            $(elem).parent().imagesLoaded().progress(function (instance, image) {
                                $(image.img).imageBlur(50);
                            });
                        });
                    }

                    $('.grid .removed img').each(function (index, elem) {
                        $(elem).parent().imagesLoaded().progress(function (instance, image) {
                            $(image.img).imageBlur(50);
                        });
                    });
                } else {
                    setLoading(false);
                    setInitializing(false);

                    if ($('.grid img').length === 0) {
                        $('.no-info').show();
                    } else {
                        $('.end-info').show();
                    }
                }
            },
            function(data) {
                $('.no-info').show();
                new ErrorToast(data);
            }
        );
    };
};