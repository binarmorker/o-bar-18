'use strict';

var RulesViewModel = function() {
    var self = this;
    self.terms = ko.observable('');
    self.privacy = ko.observable('');

    self.load = function() {
        API.Rules.Get(function(data) {
            self.terms(data.terms);
            self.privacy(data.privacy);
        },
        function(data) {
            new ErrorToast(data);
        },
        function() {
            setLoading(false);
            setInitializing(false);
        });
    }
};