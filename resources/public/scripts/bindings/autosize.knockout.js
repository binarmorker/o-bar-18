'use strict';

ko.bindingHandlers.autosize = {
    init: function(element) {
        var $element = $(element);
        autosize($element);
        var currentHeight = $element.height();
        $element.data("originalHeight", currentHeight);
    },
    update: function (element, valueAccessor) {
        var $element = $(element);
        var value = ko.unwrap(valueAccessor());

        if (value === '') {
            $element.height($element.data("originalHeight"));
        }
    }
};