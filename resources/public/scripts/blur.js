'use strict';

$.fn.imageBlur = function(radius) {
    return this.each(function (index, elem) {
        var $elem = $(elem);
        var id = Math.round(new Date().getTime() + (Math.random() * 100));
        var canvas = $('<canvas>', {id: id});

        if ($elem.siblings('canvas').length > 0) {
            $elem.siblings('canvas').remove();
        }

        canvas.insertAfter($elem);
        StackBlur.image(elem, canvas.get(0), radius);
        canvas.css({ width: '100%', height: '100%' });
    });
};