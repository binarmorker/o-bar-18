'use strict';

var Request = function (url, method, data, success, error, always) {
    var nocacheUrl = url + '?nocache=' + (new Date).getTime();

    try {
        var request = new XMLHttpRequest();

        if (method === "GET") {
            Object.entries(data).forEach(function(keyValuePair) {
                nocacheUrl += "&" + keyValuePair[0] + "=" + keyValuePair[1];
            });
            request.open(method, nocacheUrl, true);
        } else {
            request.open(method, nocacheUrl, true);
            request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
        }

        request.onload = function() {
            if (request.status >= 200 && request.status < 400) {
                if (success) {
                    success(JSON.parse(request.response));
                }
            } else {
                logErrors({
                    "message": "AJAX: " + request.statusText,
                    "url": nocacheUrl,
                    "method": method,
                    "response": request.response,
                    "data": data
                });

                if (error) {
                    error(JSON.parse(request.response));
                }
            }

            if (always) {
                always(JSON.parse(request.response));
            }
        };

        request.onerror = function() {
            logErrors({
                "message": "AJAX: " + request.statusText,
                "url": nocacheUrl,
                "method": method,
                "response": request.response,
                "data": data
            });

            /*if (Raven) {
                Raven.captureException(JSON.stringify(request), {extra: this});
            }*/

            if (error) {
                error(JSON.parse(request.response));
            }

            if (always) {
                always(JSON.parse(request.response));
            }
        };

        if (method === "GET") {
            request.send();
        } else {
            request.send(JSON.stringify(data));
        }
    } catch(e) {
        logErrors({
            "message": "Exception: " + e.message,
            "url": nocacheUrl,
            "method": method,
            "stacktrace": e.stack,
            "data": data
        });

        throw e;
    }
};