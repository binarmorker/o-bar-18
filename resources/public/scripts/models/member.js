'use strict';

var Member = function(data) {
    var self = this;
    self.username = ko.observable('');
    self.realname = ko.observable('');
    self.register_date = ko.observable('');
    self.group = ko.observable('');
    
    if (data) {
        ko.mapping.fromJS(data, {}, self);
    }
};