'use strict';

var Comment = function(data) {
    var self = this;
    self.id = ko.observable(0);
    self.post = ko.observable('');
    self.author = ko.observable(0);
    self.comment = ko.observable('').extend({ charcount: 150 });
    self.publication_date = ko.observable('');
    self.removed = ko.observable(0);
    self.upvotes = ko.observable(0);
    self.downvotes = ko.observable(0);
    self.vote = ko.observable(null);
    
    if (data) {
        ko.mapping.fromJS(data, {}, self);
    }
};