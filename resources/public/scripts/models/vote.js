'use strict';

var Vote = function(data) {
    var self = this;
    self.type = ko.observable(0);
    self.name = ko.observable('');
    
    if (data) {
        ko.mapping.fromJS(data, {}, self);
    }
};