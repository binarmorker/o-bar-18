'use strict';

var Post = function(data) {
    var self = this;
    self.id = ko.observable(0);
    self.name = ko.observable('').extend({ charcount: 50 });
    self.description = ko.observable('').extend({ charcount: 1500 });
    self.author = ko.observable(0);
    self.publication_date = ko.observable('');
    self.removed = ko.observable(0);
    self.upvotes = ko.observable(0);
    self.downvotes = ko.observable(0);
    self.vote = ko.observable(null);
    
    if (data) {
        ko.mapping.fromJS(data, {}, self);
    }
};