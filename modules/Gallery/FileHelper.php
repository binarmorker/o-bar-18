<?php
/**
 * Created by PhpStorm.
 * User: Binarmorker
 * Date: 2017-08-06
 * Time: 12:43 PM
 */

namespace Apine\Modules\Gallery;


use Apine\Application\Config;
use Apine\Exception\GenericException;
use Exception;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use FFMpeg\Filters\Video\ResizeFilter;
use FFMpeg\Format\Video\WebM;
use FFMpeg\Format\Video\X264;

class FileHelper {

    /**
     * @param UploadFile $file
     * @return UploadFile
     * @throws GenericException
     */
    public static function uploadFile(UploadFile $file) {
        $filename = uniqid();
        $ext = pathinfo($file->getName(), PATHINFO_EXTENSION);

        if (is_null($ext) || $ext == '') {
            $ext = explode('/', $file->getType())[1];
        }

        if (!is_dir('resources/public/upload')) {
            mkdir('resources/public/upload', 775);
        }

        if (strstr($file->getType(), 'video/')) {

            try {
                self::convert_video($file->getInternalName(), 'resources/public/upload/' . $filename, 720);
            } catch (Exception $e) {
                throw new GenericException("Failed to upload video", 500, $e);
            }

            try {
                self::make_video_thumbnail($file->getInternalName(), 'resources/public/upload/thumb_' . $filename . '.jpg', 250);

                $newFile = new UploadFile();
                $newFile->setName($filename);
                $newFile->setType($file->getType());
                return $newFile;
            } catch (Exception $e) {
                throw new GenericException("Failed to make thumbnail", 500, $e);
            }
        } else if (strstr($file->getType(), 'image/')) {
            try {
                self::make_image_thumbnail($file->getInternalName(), 'resources/public/upload/thumb_' . $filename . '.jpg', 250);

                rename($file->getInternalName(), 'resources/public/upload/' . $filename . '.' . $ext);
                chmod('resources/public/upload/' . $filename . '.' . $ext, 0644);

                $newFile = new UploadFile();
                $newFile->setName($filename . '.' . $ext);
                $newFile->setType($file->getType());
                return $newFile;
            } catch (Exception $e) {
                throw new GenericException("Could not generate thumbnail", 500, $e);
            }
        }

        return null;
    }

    private static function make_image_thumbnail($src, $dest, $desired_width) {
        $details = getimagesize($src);

        switch ($details[2]) {
            case 1:
                $source_image = imagecreatefromgif($src);
                break;
            case 3:
                $source_image = imagecreatefrompng($src);
                break;
            case 2:
            default:
                $source_image = imagecreatefromjpeg($src);
                break;
        }

        $width = $details[0];
        $height = $details[1];
        $desired_height = floor($height * ($desired_width / $width));
        $normalized_height = $desired_height;
        $crop = 0;

        if (($width / $height) < 0.5625) {
            $normalized_height = floor($desired_width / 0.5625);
            $crop = floor(($height - $desired_height) / 2);
        }

        $desired_height = floor($height * ($desired_width / $width));
        $virtual_image = imagecreatetruecolor($desired_width, $normalized_height);
        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, $crop / 2, $desired_width, $desired_height, $width, $height);
        return imagejpeg($virtual_image, $dest);
    }

    private static function make_video_thumbnail($src, $dest, $desired_width) {
        $ffmpeg = FFMpeg::create([
            'ffmpeg.binaries' => Config::get('runtime', 'ffmpeg_path'),
            'ffprobe.binaries' => Config::get('runtime', 'ffprobe_path'),
            'ffmpeg.threads' => 12
        ]);

        $video = $ffmpeg->open($src);
        $video->frame(TimeCode::fromSeconds(0.1))->save($dest);

        return self::make_image_thumbnail($dest, $dest, $desired_width);
    }


    private static function convert_video($src, $dest, $desired_width) {
        $ffmpeg = FFMpeg::create([
            'ffmpeg.binaries' => Config::get('runtime', 'ffmpeg_path'),
            'ffprobe.binaries' => Config::get('runtime', 'ffprobe_path'),
            'ffmpeg.threads' => 12
        ]);

        $video = $ffmpeg->open($src);
        $mp4 = new X264();
        $webm = new WebM();
        $dimension = $ffmpeg->getFFProbe()->streams($src)->videos()->first()->getDimensions();

        if ($dimension->getWidth() >= $desired_width) {
            $desired_height = floor($dimension->getHeight() * ($desired_width / $dimension->getWidth()));
            $dimension = new Dimension($desired_width, $desired_height);
        }

        $mp4
            ->setKiloBitrate(1000)
            ->setAudioCodec("libmp3lame")
            ->setAudioChannels(2);

        $webm
            ->setKiloBitrate(1000)
            ->setAudioCodec("libvorbis")
            ->setAudioChannels(2);

        $video
            ->filters()
            ->resize($dimension, ResizeFilter::RESIZEMODE_FIT, true)
            ->synchronize();

        $video
            ->save($mp4, $dest . '.mp4')
            ->save($webm, $dest . '.webm');
    }

}