<?php

namespace Apine\Modules\Gallery;

use Apine\Entity\EntityModel;
use Apine\Modules\Gallery\Enums\PublicationType;

class Report extends EntityModel {
    
    private $id;
    private $type;
    private $item_id;
    private $author;
    private $description;
    private $publication_date;
    private $publication;
    private $resolved;

    /**
     * Comment constructor.
     * @param int|null $id
     */
    public function __construct($id = null) {
        $this->_initialize('obar_reports', $id);

        if ($id != null) {
            $this->id = $id;
            $this->load();
        }
    }

    /**
     *
     */
    public function load() {
        $this->_force_loaded();
    }

    /**
     *
     */
    public function save() {
		parent::_save();
        $this->id = $this->_get_id();
    }

    /**
     *
     */
    public function delete() {
		parent::_destroy();
    }

    /**
     * @return int|null
     */
    public function get_id() {
        return $this->id;
    }

    /**
     * @param int $a_id
     */
    public function set_id($a_id) {
        $this->id = $a_id;
        $this->_set_field('id', $a_id);
    }

    /**
     * @return PublicationType
     */
    public function get_type() {
        return $this->type;
    }

    /**
     * @param PublicationType $a_type
     */
    public function set_type($a_type) {
        $this->type = $a_type;
        $this->_set_field('type', $a_type);
    }

    /**
     * @return int
     */
    public function get_item_id() {
        return $this->item_id;
    }

    /**
     * @param int $a_item_id
     */
    public function set_item_id($a_item_id) {
        $this->item_id = $a_item_id;
        $this->_set_field('item_id', $a_item_id);
    }

    /**
     * @return int
     */
    public function get_author() {
        return $this->author;
    }

    /**
     * @param int $a_author
     */
    public function set_author($a_author) {
        $this->author = $a_author;
        $this->_set_field('author', $a_author);
    }

    /**
     * @return int
     */
    public function get_description() {
        return $this->description;
    }

    /**
     * @param int $a_description
     */
    public function set_description($a_description) {
        $this->description = $a_description;
        $this->_set_field('description', $a_description);
    }

    /**
     * @return int
     */
    public function get_publication_date() {
        return $this->publication_date;
    }

    /**
     * @param int $a_publication_date
     */
    public function set_publication_date($a_publication_date) {
        $this->publication_date = $a_publication_date;
        $this->_set_field('publication_date', $a_publication_date);
    }

    /**
     * @return object
     */
    public function get_publication() {
        return $this->publication;
    }

    /**
     * @param object $a_publication
     */
    public function set_publication($a_publication) {
        $this->publication = $a_publication;
    }

    /**
     * @return bool
     */
    public function get_resolved() {
        return $this->resolved;
    }

    /**
     * @param bool $a_resolved
     */
    public function set_resolved($a_resolved) {
        $this->resolved = $a_resolved;
        $this->_set_field('resolved', $a_resolved);
    }
}