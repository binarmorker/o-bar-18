<?php
/**
 * Created by PhpStorm.
 * User: Binarmorker
 * Date: 2017-08-06
 * Time: 12:46 PM
 */

namespace Apine\Modules\Gallery;


class UploadFile {

    private $type;

    private $name;

    private $internalName;

    public function getType() {
        return $this->type;
    }

    public function setType($a_type) {
        $this->type = $a_type;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($a_name) {
        $this->name = $a_name;
    }

    public function getInternalName() {
        return $this->internalName;
    }

    public function setInternalName($a_internalName) {
        $this->internalName = $a_internalName;
    }

}