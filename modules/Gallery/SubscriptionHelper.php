<?php

namespace Apine\Modules\Gallery;

use Apine\Application\Config;
use Apine\Core\Database;
use Apine\Modules\Gallery\Enums\NotificationType;
use Apine\Modules\Gallery\Enums\PublicationType;
use Apine\Modules\Gallery\Factory\CommentFactory;
use Apine\Modules\Gallery\Factory\PostFactory;
use Apine\MVC\URLHelper;
use Apine\User\Factory\UserFactory;
use DateTime;

class SubscriptionHelper {

    public static function CreatePost(Post $post) {
        $database = new Database();

        // Check if already subscribed
        $author = $post->get_author();
        $type = PublicationType::Comment;
        $id = $post->get_id();
        $result = $database->select("SELECT * FROM obar_subscriptions 
                                     WHERE `user` = $author 
                                     AND `type` = $type 
                                     AND `publication` = $id");

        if (count($result) == 0) {
            // Create subscription
            $database->insert('obar_subscriptions', array(
                'user' => $author,
                'type' => $type,
                'publication' => $id));
        }

        $type = PublicationType::Post;
        $result = $database->select("SELECT * FROM obar_subscriptions 
                                     WHERE `user` = $author 
                                     AND `type` = $type 
                                     AND `publication` = $id");

        if (count($result) == 0) {
            // Create subscription
            $database->insert('obar_subscriptions', array(
                'user' => $author,
                'type' => $type,
                'publication' => $id));
        }
    }

    public static function CreateComment(Comment $comment) {
        $database = new Database();

        // Check if already subscribed
        $author = $comment->get_author();
        $type = PublicationType::Comment;
        $id = $comment->get_post();
        $result = $database->select("SELECT `type` FROM obar_subscriptions 
                                     WHERE `user` = $author 
                                     AND `type` = $type 
                                     AND `publication` = $id");

        if (count($result) == 0) {
            // Create subscription
            $database->insert('obar_subscriptions', array(
                'user' => $author,
                'type' => $type,
                'publication' => $id));
        }

        $subscriptions = $database->select("SELECT * FROM obar_subscriptions 
                                     WHERE `type` = $type 
                                     AND `publication` = $id");

        foreach ($subscriptions as $subscription) {
            $notification = self::CreateNotification($subscription['user'], $subscription['type'], array('post' => $comment), Config::get('runtime', 'privacy'));

            if (count($notification) > 0) {
                self::SaveNotification($subscription['user'], $notification);
            }
        }
    }

    /**
     * @param Post|Comment $post
     * @param int|null $userVote
     */
    public static function CreateVote($post, $userVote) {
        $database = new Database();

        if ($post instanceof Post) {
            $type = PublicationType::Post;
            $id = $post->get_id();
            $subscriptions = $database->select("SELECT * FROM obar_subscriptions 
                                     WHERE `type` = $type 
                                     AND `publication` = $id");
        } else {
            $type = PublicationType::Comment;
            $id = $post->get_post();
            $subscriptions = $database->select("SELECT * FROM obar_subscriptions 
                                     WHERE `type` = $type 
                                     AND `publication` = $id");
        }

        foreach ($subscriptions as $subscription) {
            $upvote = null;
            $downvote = null;
            $upvote_users = [];
            $downvote_users = [];

            if ($type == PublicationType::Post) {
                $votes = PostFactory::get_votes_for_post($post->get_id());

                foreach ($votes as $vote) {
                    if ($vote['upvote'] == 1 && $vote['author'] != $subscription['user'] && ($upvote == null || $vote['date'] > $upvote['date'])) {
                        $upvote_users[] = $vote['author'];
                        $upvote = $vote;
                    }

                    if ($vote['upvote'] == 0 && $vote['author'] != $subscription['user'] && ($downvote == null || $vote['date'] > $downvote['date'])) {
                        $downvote_users[] = $vote['author'];
                        $downvote = $vote;
                    }
                }
            } else {
                if ($subscription['user'] == $post->get_author()) {
                    foreach (CommentFactory::create_for_post($post->get_post(), []) as $iComment) {
                        if ($iComment->get_author() == $subscription['user']) {
                            $votes = PostFactory::get_votes_for_comment($iComment->get_id());

                            foreach ($votes as $vote) {
                                if ($vote['upvote'] == 1 && $vote['author'] != $subscription['user'] && ($upvote == null || $vote['date'] > $upvote['date'])) {
                                    $upvote_users[] = $vote['author'];
                                    $upvote = $vote;
                                }

                                if ($vote['upvote'] == 0 && $vote['author'] != $subscription['user'] && ($downvote == null || $vote['date'] > $downvote['date'])) {
                                    $downvote_users[] = $vote['author'];
                                    $downvote = $vote;
                                }
                            }
                        }
                    }
                }
            }

            $upvote_count = count(array_unique($upvote_users));
            $downvote_count = count(array_unique($downvote_users));

            if ($upvote != null) {
                $notification = self::CreateNotification($subscription['user'], $subscription['type'], array('post' => $post, 'vote' => $upvote, 'count' => $upvote_count), Config::get('runtime', 'privacy'));

                if ($notification != null) {
                    self::SaveNotification($subscription['user'], $notification, $subscription['user'] == $post->get_author() && $userVote == 1);
                }
            } else {
                if ($type == PublicationType::Post) {
                    self::DeleteNotification($subscription['user'], NotificationType::PostUpvote, $subscription['publication']);
                } else {
                    self::DeleteNotification($subscription['user'], NotificationType::CommentUpvote, $subscription['publication']);
                }
            }

            if ($downvote != null) {
                $notification = self::CreateNotification($subscription['user'], $subscription['type'], array('post' => $post, 'vote' => $downvote, 'count' => $downvote_count), Config::get('runtime', 'privacy'));

                if ($notification != null) {
                    self::SaveNotification($subscription['user'], $notification, $subscription['user'] == $post->get_author() && $userVote == 0);
                }
            } else {
                if ($type == PublicationType::Post) {
                    self::DeleteNotification($subscription['user'], NotificationType::PostDownvote, $subscription['publication']);
                } else {
                    self::DeleteNotification($subscription['user'], NotificationType::CommentDownvote, $subscription['publication']);
                }
            }
        }
    }

    public static function DeleteNotification($user_id, $type, $publication_id) {
        $database = new Database();
        $database->delete('obar_notifications', array('user_id' => $user_id, 'type' => $type, 'publication_id' => $publication_id));
    }

    public static function CreateNotification($user_id, $type, $data, $privacy) {
        $result = array();

        switch ($type) {
            case PublicationType::Post:
                $post = $data['post'];

                if ($post instanceof Post) {
                    $vote = $data['vote'];
                    $count = $data['count'];

                    if (!$privacy) {
                        $user = UserFactory::create_by_id($vote['author']);
                        $result['data']['author_name'] = $user->get_username();
                    }

                    if ($vote['upvote'] == 1) {
                        $result['type'] = NotificationType::PostUpvote;
                    } else {
                        $result['type'] = NotificationType::PostDownvote;
                    }

                    $result['data']['author_id'] = $vote['author'];
                    $result['data']['post_name'] = $post->get_name();
                    $result['data']['votes_count'] = $count;
                    $result['publication_id'] = $post->get_id();
                    $result['link'] = '/post/' . PseudoCrypt::hash($post->get_id());

                    $datetime = new DateTime('now');
                    $time = strtotime($vote['date']);
                    $time += $datetime->getOffset();
                    $value = date("Y-m-d H:i:s", $time);

                    $result['date'] = $value;
                }

                break;
            case PublicationType::Comment:
                $comment = $data['post'];

                if ($comment instanceof Comment) {
                    $post = PostFactory::create_by_id($comment->get_post(), array('is_admin' => true));

                    if (isset($data['vote'])) {
                        $vote = $data['vote'];
                        $count = $data['count'];
                        $author_id = $vote['author'];

                        if ($user_id == $author_id) {
                            // Comment author is excluded (does not receive notification for own comment's votes)
                            // Notifications are ignored for votes on other comments on same post
                            return null;
                        }

                        if ($vote['upvote'] == 1) {
                            $result['type'] = NotificationType::CommentUpvote;
                        } else {
                            $result['type'] = NotificationType::CommentDownvote;
                        }

                        $result['data']['votes_count'] = $count;

                        $datetime = new DateTime('now');
                        $time = strtotime($vote['date']);
                        $time += $datetime->getOffset();
                        $value = date("Y-m-d H:i:s", $time);

                        $result['date'] = $value;
                    } else {
                        $author_id = $comment->get_author();
                        $post_author_id = $post->get_author();

                        if ($user_id == $author_id) {
                            // Comment author is excluded (does not receive notification for own new comment)
                            return null;
                        }

                        if ($user_id == $post_author_id) {
                            $result['type'] = NotificationType::PostReply;
                        } else {
                            $result['type'] = NotificationType::PostComment;
                        }

                        $result['date'] = $comment->get_publication_date();
                    }

                    if (!$privacy) {
                        $user = UserFactory::create_by_id($author_id);
                        $result['data']['author_name'] = $user->get_username();
                    }

                    $result['data']['author_id'] = $author_id;
                    $result['data']['post_name'] = $post->get_name();
                    $result['publication_id'] = $post->get_id();
                    $result['link'] = '/post/' . PseudoCrypt::hash($post->get_id()) . '/' . $comment->get_id();
                }

                break;
        }

        return $result;
    }

    public static function SaveNotification($user, $notification, $notify = true) {
        $database = new Database();
        $data = array();
        $link = null;
        $type = $notification['type'];
        $publication_id = $notification['publication_id'];
        $link = $database->quote($notification['link']);
        $date = $database->quote($notification['date']);

        switch ($type) {
            case NotificationType::PostReply:
            case NotificationType::PostComment:
                $data['author_id'] = $notification['data']['author_id'];
                break;
            case NotificationType::PostUpvote:
            case NotificationType::PostDownvote:
            case NotificationType::CommentUpvote:
            case NotificationType::CommentDownvote:
                $data['author_id'] = $notification['data']['author_id'];
                $data['votes_count'] = $notification['data']['votes_count'];
                break;
        }

        $encoded_data = $database->quote(json_encode($data));

        $tokens = $database->select("SELECT * FROM `obar_messaging`
                                              WHERE `user_id` = $user");

        $results = $database->select("SELECT * FROM `obar_notifications`
                                            WHERE `user_id` = $user
                                            AND `type` = $type
                                            AND `publication_id` = $publication_id");

        if ($notify && count($tokens) > 0 && (count($results) == 0 || $results[0]['seen'] == 1)) {
            $notificationMessage = self::GetNotificationMessage($type, $notification['data']);

            foreach ($tokens as $token) {
                $url = 'https://fcm.googleapis.com/fcm/send';
                $fields = json_encode([
                    'to' => $token['token'],
                    'notification' => [
                        'title' => Config::get('application', 'title') . ' ' . Config::get('application', 'subtitle'),
                        'icon' => '/resources/public/assets/small_logo.png',
                        'body' => $notificationMessage,
                        'click_action' => URLHelper::path(trim($notification['link'], '/')),
                        'data' => [
                            'id' => $publication_id,
                            'type' => $type
                        ]
                    ]

                ]);
                $headers = [
                    'Authorization: key=' . Config::get('runtime', 'firebase_key'),
                    'Content-Type: application/json'
                ];

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

                $result = curl_exec($ch);

                if ($result === false) {
                    error_log(curl_error($ch));
                } else {
                    error_log($result);
                }

                curl_close($ch);
            }
        }

        if ($notify) {
            $database->exec("REPLACE INTO `obar_notifications` 
                                  (`user_id`, `type`, `publication_id`, `link`, `data`, `date`, `seen`)
                                  VALUE ($user, $type, $publication_id, $link, $encoded_data, $date, 0)");
        } else {
            $database->exec("REPLACE INTO `obar_notifications` 
                                  (`user_id`, `type`, `publication_id`, `link`, `data`, `date`, `seen`)
                                  VALUE ($user, $type, $publication_id, $link, $encoded_data, $date, 1)");
        }
    }

    public static function GetNotificationMessage($type, $data) {
        $notificationMessage = '';
        $avatar = null;

        switch ($type) {
            case NotificationType::PostReply:
                if ($data['author_name']) {
                    $notificationMessage = $data['author_name'];
                } else {
                    $notificationMessage = 'A user';
                }

                $notificationMessage .= ' commented on your post: "' . $data['post_name'] . '".';

                break;
            case NotificationType::PostComment:
                if ($data['author_name']) {
                    $notificationMessage = $data['author_name'];
                } else {
                    $notificationMessage = 'A user';
                }

                $notificationMessage .= ' commented on a post you are following: "' . $data['post_name'] . '".';

                break;
            case NotificationType::PostUpvote:
                if ($data['author_name']) {
                    if ($data['votes_count'] > 1) {
                        $notificationMessage = $data['author_name'] . ' and ' . ($data['votes_count'] - 1) . ' other user' . ($data['votes_count'] - 1 < 2 ? '' : 's');
                    } else {
                        $notificationMessage = $data['author_name'];
                    }
                } else {
                    $notificationMessage = $data['votes_count'] . ' users';
                }

                $notificationMessage .= ' liked your post: "' . $data['post_name'] . '".';

                break;
            case NotificationType::PostDownvote:
                if ($data['author_name']) {
                    if ($data['votes_count'] > 1) {
                        $notificationMessage = $data['author_name'] . ' and ' . ($data['votes_count'] - 1) . ' other user' . ($data['votes_count'] - 1 < 2 ? '' : 's');
                    } else {
                        $notificationMessage = $data['author_name'];
                    }
                } else {
                    $notificationMessage = $data['votes_count'] . ' users';
                }

                $notificationMessage .= ' hated your post: "' . $data['post_name'] . '".';

                break;
            case NotificationType::CommentUpvote:
                if ($data['author_name']) {
                    if ($data['votes_count'] > 1) {
                        $notificationMessage = $data['author_name'] . ' and ' . ($data['votes_count'] - 1) . ' other user' . ($data['votes_count'] - 1 < 2 ? '' : 's');
                    } else {
                        $notificationMessage = $data['author_name'];
                    }
                } else {
                    $notificationMessage = $data['votes_count'] . ' users';
                }

                $notificationMessage .= ' liked your comment: "' . $data['post_name'] . '".';

                break;
            case NotificationType::CommentDownvote:
                if ($data['author_name']) {
                    if ($data['votes_count'] > 1) {
                        $notificationMessage = $data['author_name'] . ' and ' . ($data['votes_count'] - 1) . ' other user' . ($data['votes_count'] - 1 < 2 ? '' : 's');
                    } else {
                        $notificationMessage = $data['author_name'];
                    }
                } else {
                    $notificationMessage = $data['votes_count'] . ' users';
                }

                $notificationMessage .= ' hated your comment: "' . $data['post_name'] . '".';

                break;
        }

        return $notificationMessage;
    }

    public static function AddSubscription($userid, $subscription) {
        $database = new Database();
        $database->prepare("REPLACE INTO `obar_messaging`
                                    (`user_id`, `token`)
                                    VALUES (?, ?)");
        $database->execute([$userid, $subscription]);
    }

    public static function GetAllUnreadNotificationsForPostAndUser($post_id, $user_id) {
        $database = new Database();
        $results = $database->select("SELECT * FROM `obar_notifications`
                                 WHERE `user_id` = $user_id
                                 AND `publication_id` = $post_id
                                 AND `seen` = 0");
        return $results;
    }

}