<?php

namespace Apine\Modules\Gallery\Enums;

class NotificationType {
    const None = 0;
    const PostReply = 1;
    const PostComment = 2;
    const PostUpvote = 3;
    const PostDownvote = 4;
    const CommentUpvote = 5;
    const CommentDownvote = 6;
}