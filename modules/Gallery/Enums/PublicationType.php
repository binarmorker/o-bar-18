<?php

namespace Apine\Modules\Gallery\Enums;

class PublicationType {
    const None = 0;
    const Post = 1;
    const Comment = 2;
    const Message = 3;
}