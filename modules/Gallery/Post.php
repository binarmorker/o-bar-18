<?php

namespace Apine\Modules\Gallery;

use Apine\Entity\EntityModel;

class Post extends EntityModel {
    
    private $id;
    private $name;
    private $description;
    private $author;
    private $publication_date;
    private $elements;
    private $removed;
    private $upvotes;
    private $downvotes;
    private $mature;

    /**
     * Post constructor.
     * @param int|null $id
     */
    public function __construct($id = null) {
        $this->_initialize('obar_posts', $id);

        if ($id != null) {
            $this->id = $id;
            $this->load();
        }
    }

    /**
     *
     */
    public function load() {
        $this->_force_loaded();
    }

    /**
     *
     */
    public function save() {
		parent::_save();
        $this->id = $this->_get_id();
    }

    /**
     *
     */
    public function delete() {
		parent::_destroy();
    }

    /**
     * @return int|null
     */
    public function get_id() {
        return $this->id;
    }

    /**
     * @param int $a_id
     */
    public function set_id($a_id) {
        $this->id = $a_id;
        $this->_set_field('id', $a_id);
    }

    /**
     * @return string
     */
    public function get_name() {
        return $this->name;
    }

    /**
     * @param string $a_name
     */
    public function set_name($a_name) {
        $this->name = $a_name;
        $this->_set_field('name', $a_name);
    }

    /**
     * @return string
     */
    public function get_description() {
        return $this->description;
    }

    /**
     * @param string $a_description
     */
    public function set_description($a_description) {
        $this->description = $a_description;
        $this->_set_field('description', $a_description);
    }

    /**
     * @return int
     */
    public function get_author() {
        return $this->author;
    }

    /**
     * @param int $a_author
     */
    public function set_author($a_author) {
        $this->author = $a_author;
        $this->_set_field('author', $a_author);
    }

    /**
     * @return string
     */
    public function get_publication_date() {
        return $this->publication_date;
    }

    /**
     * @param string $a_publication_date
     */
    public function set_publication_date($a_publication_date) {
        $this->publication_date = $a_publication_date;
        $this->_set_field('publication_date', $a_publication_date);
    }

    /**
     * @return Element[]
     */
    public function get_elements() {
        return $this->elements;
    }

    /**
     * @param Element[] $a_elements
     */
    public function set_elements($a_elements) {
        $this->elements = $a_elements;
    }

    /**
     * @return int
     */
    public function get_upvotes() {
        return $this->upvotes;
    }

    /**
     * @param int $a_upvotes
     */
    public function set_upvotes($a_upvotes) {
        $this->upvotes = $a_upvotes;
    }

    /**
     * @return int
     */
    public function get_downvotes() {
        return $this->downvotes;
    }

    /**
     * @param int $a_downvotes
     */
    public function set_downvotes($a_downvotes) {
        $this->downvotes = $a_downvotes;
    }

    /**
     * @return bool
     */
    public function get_mature() {
        return $this->mature;
    }

    /**
     * @param bool $a_mature
     */
    public function set_mature($a_mature) {
        $this->mature = $a_mature;
        $this->_set_field('mature', $a_mature);
    }

    /**
     * @return bool
     */
    public function get_removed() {
        return $this->removed;
    }

    /**
     * @param bool $a_removed
     */
    public function set_removed($a_removed) {
        $this->removed = $a_removed;
        $this->_set_field('removed', $a_removed);
    }
    
}